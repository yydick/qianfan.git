<?php

// Copyright 2024 yydick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Yydick\Qianfan\Core\Auth;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Yydick\Qianfan\Interfaces\AuthInterface;

class QianfanOAuth implements AuthInterface
{
    public const AUTH_URL = "https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=%s&client_secret=%s";
    public const ACCESS_TOKEN = "access_token";
    public const EXPIRE_OFFSET_SECONDS = 10;
    private string $token;
    private readonly string $tokenCacheKey;
    public function __construct(protected readonly string $apiKey = "", protected readonly string $secretKey = "")
    {
        $this->tokenCacheKey = "qianfan_token_" . config("app.name") . "_" . config("app.env");
    }
    public function signRequest(Request $request): Request
    {
        $url = $this->sign();
        dd($url);
        return $request;
    }

    public function sign(): string
    {
        $url = sprintf(self::AUTH_URL, $this->apiKey, $this->secretKey);
        // $client = new Client();
        // $response = $client->post($url);
        return $url;
    }
}
